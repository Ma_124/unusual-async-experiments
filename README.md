# Unusual Async Experiments
These experiments use [Rust]'s `async` feature in unusual ways.

One inspiration to create these experiments was [“How to think about `async`/`await` in Rust”][async-inversion] by Cliffle.

[Rust]: https://rust-lang.org/
[async-inversion]: https://cliffle.com/blog/async-inversion/

## Coroutines ([Source Code][coroutine-v1])
The Rust community [works][rust-blog-coroutines] on bringing coroutines and generators into the Rust language.
Until then, we can use `async` to build our own coroutine system.
One thing we can do with it, is implementing Python-style generators:

``` rust
pub fn even_digits() -> Coroutine<usize, ()> {
    co! { cx ->
        let mut i = 0;
        while i < 10 {
            cx.yeet(i).await;   // yield is already a reserved keyword in Rust
            i += 2;
        }
    }
}

pub fn odd_digits() -> Coroutine<usize, ()> {
    co! { cx ->
        let even = even_digits();
        for i in even {
            cx.yeet(i + 1).await;
        }
    }
}

assert_eq!(
    odd_digits().collect::<Vec<_>>(),
    [1, 3, 5, 7, 9].to_vec(),
);
```

A slightly less trivial example use of `yield` is traversing a tree in order:

``` rust
#[async_recursion(?Send)]
async fn co_inorder(self, cx: &Context<T>) {
    if let Some(left) = self.left {
        left.co_inorder(cx).await;
    }

    cx.yeet(self.value).await;

    if let Some(right) = self.right {
        right.co_inorder(cx).await;
    }
}

pub fn inorder(self) -> impl Iterator<Item = T> {
    co! {
        cx ->
        self.co_inorder(&cx).await
    }
}
```

While this is possible on stable Rust without any use of unsafe [^1],
proper integration of coroutines would make them easier to work with and more performant.

[^1]: at least when [`Waker::noop`] is stabilized

[coroutine-v1]: src/coroutine/v1.rs
[rust-blog-coroutines]: https://blog.rust-lang.org/inside-rust/2023/10/23/coroutines.html
[`Waker::noop`]: https://doc.rust-lang.org/stable/std/task/struct.Waker.html#method.noop

## Parser Combinator ([Source Code][parser-v1])
Having implemented coroutines, we can use them to build parsers.
In this repo, I have only implemented a simple string matcher based on
[“Revisiting Coroutines”][coroutines-revisited] by de Moura and Ierusalimschy.

``` rust
let parser = seq(alt(just("abc"), just("de")), just("x"));

assert!(matches("abcx".into(), parser));
assert!(matches("dex".into(), parser));

assert!(!matches("abc".into(), parser));
```

The current implementation isn't optimized and sidesteps having to think about lifetimes by using a bunch of `&'static` and `Rc`.

[parser-v1]: src/parser/v1.rs
[coroutines-revisited]: https://doi.org/10.1145/1462166.1462167
