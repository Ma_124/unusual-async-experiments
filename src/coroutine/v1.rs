//! Implementing asymmetric, kinda first-class, stackful coroutines.
//!
//! In Rust 2021, `yield` is a reserved keyword, thus we replace it with `yeet` in this code.
//!
//! Definitions: <https://doi.org/10.1145/1462166.1462167>

use std::{
    cell::Cell,
    marker::PhantomData,
    pin::Pin,
    ptr,
    rc::Rc,
    sync::OnceLock,
    task::{Poll, RawWaker, RawWakerVTable, Waker},
};

use async_recursion::async_recursion;
use futures::{pending, Future, FutureExt};

pub enum Yeet<Y, T> {
    Yeet(Y),
    Terminate(T),
}

pub type Fut<O = ()> = Pin<Box<dyn Future<Output = O>>>;

pub struct Coroutine<Y, T> {
    f: Pin<Box<dyn Future<Output = T>>>,
    yeeted: Rc<Cell<Option<Y>>>,
    _pd: PhantomData<(Y, T)>,
}

impl<Y, T> Coroutine<Y, T> {
    pub fn create(f: impl FnOnce(Context<Y>) -> Pin<Box<dyn Future<Output = T>>>) -> Self {
        let yeeted = Rc::new(Cell::new(None));

        let context = Context {
            yeeted: yeeted.clone(),
            _pd: PhantomData,
        };

        Self {
            f: f(context),
            yeeted,
            _pd: PhantomData,
        }
    }

    pub fn resume(&mut self) -> Yeet<Y, T> {
        assert!(
            self.yeeted.take().is_none(),
            "yeeted values must be consumed before resuming"
        );
        match self.f.poll_unpin(&mut noop_context()) {
            Poll::Ready(t) => Yeet::Terminate(t),
            Poll::Pending => Yeet::Yeet(
                self.yeeted
                    .take()
                    .expect("futures must not return Pending without yeeting"),
            ),
        }
    }
}

impl<Y> Iterator for Coroutine<Y, ()> {
    type Item = Y;

    fn next(&mut self) -> Option<Self::Item> {
        match self.resume() {
            Yeet::Yeet(y) => Some(y),
            Yeet::Terminate(()) => None,
        }
    }
}

#[derive(Clone)]
pub struct Context<Y> {
    yeeted: Rc<Cell<Option<Y>>>,
    _pd: PhantomData<Y>,
}

impl<Y> Context<Y> {
    pub async fn yeet(&self, value: Y) {
        self.yeeted.set(Some(value));
        pending!()
    }
}

macro_rules! co {
    ($cx:ident -> $($t:tt)*) => {
        Coroutine::create(|$cx| {
            Box::pin(async move {
                $($t)*
            })
        })
    };
}

pub fn even_digits() -> Coroutine<usize, ()> {
    co! {
        cx ->
        let mut i = 0;
        while i < 10 {
            cx.yeet(i).await;
            i += 2;
        }
    }
}

pub fn odd_digits() -> Coroutine<usize, ()> {
    co! {
        cx ->
        let even = even_digits();

        for i in even {
            cx.yeet(i + 1).await;
        }
    }
}

#[test]
fn test_even_digits() {
    assert_eq!([0, 2, 4, 6, 8].to_vec(), even_digits().collect::<Vec<_>>());
}

#[test]
fn test_odd_digits() {
    assert_eq!([1, 3, 5, 7, 9].to_vec(), odd_digits().collect::<Vec<_>>());
}

pub struct Tree<T> {
    value: T,
    left: Option<Box<Tree<T>>>,
    right: Option<Box<Tree<T>>>,
}

impl<T: 'static> Tree<T> {
    pub fn leaf(value: T) -> Tree<T> {
        Tree {
            value,
            left: None,
            right: None,
        }
    }

    pub fn inner(left: Tree<T>, value: T, right: Tree<T>) -> Tree<T> {
        Tree {
            value,
            left: Some(Box::new(left)),
            right: Some(Box::new(right)),
        }
    }

    #[async_recursion(?Send)]
    async fn co_inorder(self, cx: &Context<T>) {
        if let Some(left) = self.left {
            left.co_inorder(cx).await;
        }

        cx.yeet(self.value).await;

        if let Some(right) = self.right {
            right.co_inorder(cx).await;
        }
    }

    pub fn inorder(self) -> impl Iterator<Item = T> {
        co! {
            cx ->
            self.co_inorder(&cx).await
        }
    }
}

#[test]
fn test_inorder() {
    let tree = Tree::<u32>::inner(
        Tree::inner(Tree::leaf(1), 2, Tree::leaf(3)),
        4,
        Tree::leaf(5),
    );

    assert_eq!([1, 2, 3, 4, 5].to_vec(), tree.inorder().collect::<Vec<_>>());
}

pub fn noop_waker() -> Waker {
    const VTABLE: RawWakerVTable = RawWakerVTable::new(
        // Cloning just returns a new no-op raw waker
        |_| RAW,
        // `wake` does nothing
        |_| {},
        // `wake_by_ref` does nothing
        |_| {},
        // Dropping does nothing as we don't allocate anything
        |_| {},
    );
    const RAW: RawWaker = RawWaker::new(ptr::null(), &VTABLE);

    unsafe { Waker::from_raw(RAW) }
}

fn noop_context() -> std::task::Context<'static> {
    static WAKER: OnceLock<Waker> = OnceLock::new();

    std::task::Context::from_waker(WAKER.get_or_init(noop_waker))
}
