//! Implementing the string pattern matching example from <https://doi.org/10.1145/1462166.1462167>.

use std::rc::Rc;

use crate::coroutine::v1::{Context, Coroutine, Fut};

pub trait Parser {
    fn parse(&self, cx: Context<usize>, s: Rc<str>, pos: usize) -> Fut;
}

impl<T: Fn(Context<usize>, Rc<str>, usize) -> Fut> Parser for T {
    fn parse(&self, cx: Context<usize>, s: Rc<str>, pos: usize) -> Fut {
        self(cx, s, pos)
    }
}

macro_rules! p {
    ($cx:ident, $s:ident, $pos:ident $(move)? -> $($t:tt)*) => {
        Box::leak(Box::new(
            move |$cx: Context<usize>, $s: Rc<str>, $pos: usize| -> Fut {
                Box::pin(async move {
                    $($t)*
                })
            }
        ))
    };
}

pub fn just(pattern: &'static str) -> &'static impl Parser {
    p! {
        cx, s, pos ->
        if s[pos..].starts_with(pattern) {
            cx.yeet(pos + pattern.len()).await;
        }
    }
}

pub fn alt(a: &'static impl Parser, b: &'static impl Parser) -> &'static impl Parser {
    p! {
        cx, s, pos ->
        a.parse(cx.clone(), s.clone(), pos).await;
        b.parse(cx.clone(), s.clone(), pos).await;
    }
}

pub fn seq(first: &'static impl Parser, second: &'static impl Parser) -> &'static impl Parser {
    p! {
        cx, s, pos ->
        let btpoint = Coroutine::create(|cx| first.parse(cx, s.clone(), pos));
        for npos in btpoint {
            second.parse(cx.clone(), s.clone(), npos).await;
        }
    }
}

pub fn matches(s: Rc<str>, p: &'static impl Parser) -> bool {
    let m = Coroutine::create(|cx| p.parse(cx.clone(), s.clone(), 0));
    for pos in m {
        if pos == s.len() {
            return true;
        }
    }

    false
}

#[test]
fn test_parse() {
    let parser = seq(alt(just("abc"), just("de")), just("x"));

    assert!(matches("abcx".into(), parser));
    assert!(matches("dex".into(), parser));

    assert!(!matches("abc".into(), parser));
}
